<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Task extends Model
{
    public function project(){
    	return $this->belongsTo(Project::class);
    }

    public function employee(){
    	return $this->belongsTo(User::class,'assigned_to');
    }

    public function timing() {
    	return $this->hasMany(Timing::class,'task_id');
    }

public function getHours() {
    Carbon::parse($this->end_datetime)->diffInHours(Carbon::parse($this->start_datetime));

}

}
