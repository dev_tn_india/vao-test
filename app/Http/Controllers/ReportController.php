<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Project;
use App\Timing;
use App\User;
use App\Team;
use App\Task;
class ReportController extends Controller
{

private $employees;
private $projects;
  public function __construct() {
		$this->employees = User::where('is_admin', 0)->get();
		$this->projects = Project::all();
    }

	 public function index()
    {
    
      // return  Carbon::parse($timings->end_datetime)->diffInHours(Carbon::parse($timings->start_datetime));
        return view('home')->with(['employees'=>$this->employees, 'projects'=>$this->projects]);
    }

    public function getResults(Request $request) {
    	$taksbymonth = Timing::whereMonth('start_datetime', '=', $request->month)->get();

    	foreach ($taksbymonth as $task) {

	    	$tasks = Task::findOrFail($task->task_id)
	    					->where('project_id',$request->project_id)
	    					->where('assigned_to',$request->employee_id)
	    					->get();
	    				//	$task.push('total',40);



    	}


    	return view('home')
    			->with(['tasks'=>$tasks,
    					'employees'=>$this->employees,
    					'projects'=>$this->projects]);

    }


}
