<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Timing;
use App\Project;
use App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = User::where('id', '<>', Auth::id())->get();
        $projects = Project::all();
       $timings = Timing::find(1);
      // return  Carbon::parse($timings->end_datetime)->diffInHours(Carbon::parse($timings->start_datetime));
        return view('home',compact('employees','projects'));
    }
}
