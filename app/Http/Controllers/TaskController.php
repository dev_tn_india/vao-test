<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Project;
use App\Task;
use App\User;
class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();
        $projects = Project::all();
        $employees = User::all();
        return view('tasks', compact('tasks','projects','employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
           $this->validate($request, [
            'title' => 'required|string|max:25',
            'description' => 'required|string',
            ]);

            $task = new Task;
            $task->title = $request->title;
            $task->description = $request->description;
            $task->project_id = $request->project_id;
            $task->created_by = Auth::id();
            $task->assigned_to = $request->assigned_to;
            $task->status_id = 0;
            $task->save();

            return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                
           $this->validate($request, [
            'title' => 'required|string|max:25',
            'description' => 'required|string',
            ]);

            $task = Task::findOrFail($id);
            $task->title = $request->title;
            $task->description = $request->description;
            $task->project_id = $request->project_id;
            $task->created_by = $request->created_by;
            $task->assigned_to = $request->assigned_to;
            $task->status_id = $request->status_id;
            $task->save();

            return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
           Task::findOrFail($id)->delete()? "deleted" : "problem";
        return back();
    }
}
