<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Project;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        return view('projects', compact('projects'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

           $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'required|string|min:2',
            ]);

            $project = new Project;
            $project->name = $request->name;
            $project->description = $request->description;
            $project->created_by = Auth::id();
            $project->start_at =$request->start_at;
            $project->end_at =$request->end_at;
            $project->save();

            return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $project = Project::findOrFail($id);
        return view('edit_project', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
           $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'required|string|min:2',
            ]);

            $project = Project::findOrFail($id);
            $project->name = $request->name;
            $project->description = $request->description;
            $project->start_at =$request->start_at;
            $project->end_at =$request->end_at;
            $project->save();

            return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
           Project::findOrFail($id)->delete()? "deleted" : "problem";
        return back();
    }
}
