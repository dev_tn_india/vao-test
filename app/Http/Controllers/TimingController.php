<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Timing;
class TimingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $timings = Timing::all();
        $timings = Timing::find(1);
       return  $timings->end_datetime->diffInSeconds($timing->start_datetime)
    //    return view('timings', compact('timings'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
           $this->validate($request, [
            'pause_datetime' => 'nullable',
            'end_datatime' => 'nullable',
            ]);

            $timing = new Timing;
            $timing->task_id = $request->task_id;
            $timing->start_datetime = $request->start_datetime;
            $timing->pause_datetime =$request->pause_datetime;
            $timing->end_datetime =$request->end_datetime;
            $timing->save();

            return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$id = task id
        $this->validate($request, [
            'pause_datetime' => 'nullable',
            'end_datatime' => 'nullable',
            ]);

            $timing = Timing::where('task_id',$id);
            $timing->pause_datetime =$request->pause_datetime;
            $timing->end_datetime =$request->end_datetime;
            $timing->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
