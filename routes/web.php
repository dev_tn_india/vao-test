<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin_template');
});

Auth::routes();

Route::get('/home', 'ReportController@index')->name('home');
Route::get('/reports', 'ReportController@getResults');

Route::get('/users', 'UserController@index');
Route::post('/users', 'UserController@store');
Route::get('/users/edit/', 'UserController@edit');
Route::delete('/users/{id}', 'UserController@destroy');

Route::get('/projects', 'ProjectController@index');
Route::post('/projects', 'ProjectController@store');
Route::delete('/projects/{id}', 'ProjectController@destroy');

Route::get('/tasks', 'TaskController@index');
Route::post('/tasks', 'TaskController@store');
Route::delete('/tasks/{id}', 'TaskController@destroy');

Route::get('/teams', 'TeamController@index');
Route::post('/teams', 'TeamController@store');
Route::delete('/teams/{id}', 'TeamController@destroy');

