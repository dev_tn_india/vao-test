@extends('admin_template')
@section('content')
	<div class="row" id="users-list">

    <div class="col-md-12">
          <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">New User</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <form action="{{action('UserController@store')}}" method="post">
                        {{ csrf_field() }}
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Name</label>
                <input class="form-control" type="text" name="name"></input>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Email</label>
                <input class="form-control" type="email" name="email"></input>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Team</label>
                <select class="form-control" style="width: 100%;" name="team_id">
                  @foreach($teams as $team)
                  <option value="{{$team->id}}">{{$team->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
              <!-- /.form-group -->
            </div>
          </div>
          <!-- /.box body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-info">Add</button>
        </div>
      </form>
      </div>
    </div>
		<div class="col-md-12">
			<div class="box">
            <div class="box-header">
              <h3 class="box-title">Users List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tab" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Team</th>
                  <th>Created at</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </thead>
                <tbody>
              @if(empty($users))
 					    #No data
              @else
                @foreach ($users as $user)
	                <tr>
	                  <td># {{ $user->id }}</td>
	                  <td>{{ $user->name }}</td>
	                  <td>{{$user->email}}</td>
	                  <td>{{$user->team->name}}</td>
	                  <td>{{$user->created_at->toDayDateTimeString()}}</td>
	                  <td>
  	                  <form method="POST" action="users/{{ $user->id }}" class="delpage">
  	                   {{ csrf_field() }}
  	                  <input name="_method" type="hidden" value="DELETE">
  	                  	<button type="submit" class="delbtn">
  	                  		<i class="fa fa-trash"></i>
  	                  	</button>
  	                  </form>
	             	   </td>
	                </tr>
				@endforeach	
              @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Team</th>
                  <th>Created at</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
		</div>
	</div>
@endsection
