@extends('admin_template')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
                      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Search</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <form action="{{action('ReportController@getResults')}}" method="get">
                        {{ csrf_field() }}
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Month</label>
                <select class="form-control" style="width: 100%;" name="month">
                
                  <?php 
for ($m=1; $m<=12; $m++) {
     $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
    echo "<option value=$m>$month</option>";
     }
                   ?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Employee</label>
                <select class="form-control" style="width: 100%;" name="employee_id">
                  @foreach($employees as $employee)
                  <option value="{{$employee->id}}">{{$employee->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Project</label>
                <select class="form-control" style="width: 100%;" name="project_id">
                  @foreach($projects as $project)
                  <option value="{{$project->id}}">{{$project->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
              <!-- /.form-group -->
            </div>
          </div>
          <!-- /.box body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-info">Search</button>
        </div>
      </form>
      </div>

        </div>


                <div class="col-md-12">
            <div class="box">
            <div class="box-header">
              <h3 class="box-title">Tasks List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tab" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>project</th>
                  <th>Employee</th>
                  <th>Working Hours</th>
                  <th>Created at</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </thead>
                <tbody>
              @if(empty($tasks))
                        #No data
              @else
                @foreach ($tasks as $task)
                    <tr>
                      <td># {{ $task->id }}</td>
                      <td>{{ $task->title }}</td>
                      <td>{{$task->description}}</td>
                    <td>{{$task->project->name}}</td>
                      <td>{{$task->employee->name}}</td>
                      <td>{{$task->timing}}</td>
                      <td>{{$task->created_at->toDayDateTimeString()}}</td>
                      <td>
                      <form method="POST" action="tasks/{{ $task->id }}" class="delpage">
                       {{ csrf_field() }}
                      <input name="_method" type="hidden" value="DELETE">
                        <button type="submit" class="delbtn">
                            <i class="fa fa-trash"></i>
                        </button>
                      </form>
                       </td>
                    </tr>
                @endforeach 
              @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>project</th>
                  <th>Employee</th>
                  <th>Working Hours</th>
                  <th>Created at</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
        </div>
    </div>
</div>
@endsection
