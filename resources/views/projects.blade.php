@extends('admin_template')
@section('content')
	<div class="row" id="projects-list">

    <div class="col-md-12">
          <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">New Project</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <form action="{{action('ProjectController@store')}}" method="post">
                        {{ csrf_field() }}
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Name</label>
                <input class="form-control" type="text" name="name"></input>
              </div>
              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="description" rows="2"></textarea>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Start date</label>
                <input type="date" name="start_at" class="form-control">
              </div>
              <div class="form-group">
                <label>Deadline date</label>
                <input type="date" name="end_at" class="form-control">
              </div>
            </div>
              <!-- /.form-group -->
            </div>
          </div>
          <!-- /.box body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-info">Add</button>
        </div>
      </form>
      </div>
    </div>
		<div class="col-md-12">
			<div class="box">
            <div class="box-header">
              <h3 class="box-title">projects List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tab" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Start at</th>
                  <th>End at</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </thead>
                <tbody>
              @if(empty($projects))
 					    #No data
              @else
                @foreach ($projects as $project)
	                <tr>
	                  <td># {{ $project->id }}</td>
	                  <td>{{ $project->name }}</td>
	                  <td>{{$project->description}}</td>
                    <td>{{$project->start_at}}</td>
	                  <td>{{$project->end_at}}</td>
	                  <td>
  	                  <form method="POST" action="projects/{{ $project->id }}" class="delpage">
  	                   {{ csrf_field() }}
  	                  <input name="_method" type="hidden" value="DELETE">
  	                  	<button type="submit" class="delbtn">
  	                  		<i class="fa fa-trash"></i>
  	                  	</button>
  	                  </form>
	             	   </td>
	                </tr>
				@endforeach	
              @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Start at</th>
                  <th>End at</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
		</div>
	</div>
@endsection
