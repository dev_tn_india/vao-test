@extends('admin_template')
@section('content')
	<div class="row" id="teams-list">

    <div class="col-md-12">
          <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">New Team</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <form action="{{action('TeamController@store')}}" method="post">
                        {{ csrf_field() }}
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Name</label>
                <input class="form-control" type="text" name="name"></input>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="description" rows="2"></textarea>
              </div>
            </div>
              <!-- /.form-group -->
            </div>
          </div>
          <!-- /.box body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-info">Add</button>
        </div>
      </form>
      </div>
    </div>
		<div class="col-md-12">
			<div class="box">
            <div class="box-header">
              <h3 class="box-title">Teams List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tab" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Members</th>
                  <th>Created at</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </thead>
                <tbody>
              @if(empty($teams))
 					    #No data
              @else
                @foreach ($teams as $team)
	                <tr>
	                  <td># {{ $team->id }}</td>
	                  <td>{{ $team->name }}</td>
	                  <td>{{$team->description}}</td>
	                  <td></td>
	                  <td>{{$team->created_at->toDayDateTimeString()}}</td>
	                  <td>
  	                  <form method="POST" action="teams/{{ $team->id }}" class="delpage">
  	                   {{ csrf_field() }}
  	                  <input name="_method" type="hidden" value="DELETE">
  	                  	<button type="submit" class="delbtn">
  	                  		<i class="fa fa-trash"></i>
  	                  	</button>
  	                  </form>
	             	   </td>
	                </tr>
				@endforeach	
              @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Members</th>
                  <th>Created at</th>
                  <th><i class="fa fa-trash"></i></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
		</div>
	</div>
@endsection
